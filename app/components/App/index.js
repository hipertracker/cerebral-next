// eslint-disable-next-line
import React, {Component, PropTypes} from 'react'
import {connect} from 'cerebral-view-react'

class App extends Component {
  render() {
    return (
      <div>
        <p>Global state: {this.props.msg1}</p>
        <p>Module state: {this.props.msg2}</p>
      </div>
    )
  }
}

App.propTypes = {
  msg1: PropTypes.string.isRequired,
  msg2: PropTypes.string.isRequired
}

export default connect({
  msg1: 'globalMessage',
  msg2: 'app.moduleMessage'
}, App)

